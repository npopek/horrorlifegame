package casir.lifegame;

public class LifeGame
{
    private Board cellBoard;
    private Board aliveCellBoard;
    private int boardsSize;
    
    public LifeGame(int size)
    {
        cellBoard = new Board(size);
        aliveCellBoard = new Board(size);
        this.boardsSize = size;
        
        initialiseStartingAliveCells();
    }
    
    public void startGame()
    {
        while (!isGameOver())
        {
            UserInterface.displayGameBoard(aliveCellBoard);
            
            try
            {
                wait(100);
            }
            catch (InterruptedException ex)
            {
                System.out.println("Execution has been halted due to exception. " + ex.getMessage());
                return;
            }
            
            cellBoard = aliveCellBoard.clone();
            aliveCellBoard = new Board(this.boardsSize);
            
            for (int x = 0; x < this.boardsSize; x++)
            {
                for (int y = 0; y < this.boardsSize; y++)
                {
                    int amountOfAdjascentLivingCells = getLivingAdjascentCellsCountAt(x, y);
                    
                    //La cellule vie si elle est déjà en vie et a exactement 2 ou 3 cellules parents.
                    //Une cellule morte revie si il y a exactement trois parents.
                    //Sinon elles meurent.
                    boolean isAlive = (!cellBoard.isCellAlive(x, y) && amountOfAdjascentLivingCells == 3)
                            ||
                            (
                                cellBoard.isCellAlive(x, y) && (amountOfAdjascentLivingCells == 3
                                        || amountOfAdjascentLivingCells == 2)
                            );
                    
                    aliveCellBoard.setAliveStateForCell(x, y, isAlive);
                }
            }
        }
        
        UserInterface.displayEndMessage();
    }
    
    private void initialiseStartingAliveCells()
    {
        aliveCellBoard.setAliveStateForCell(17, 17, true);
        aliveCellBoard.setAliveStateForCell(18, 17, true);
        aliveCellBoard.setAliveStateForCell(17, 18, true);
        aliveCellBoard.setAliveStateForCell(19, 18, true);
        aliveCellBoard.setAliveStateForCell(17, 19, true);
    }
    
    private boolean isGameOver()
    {
        return aliveCellBoard.equals(cellBoard);
    }
    
    private void wait(int milliSeconds) throws InterruptedException
    {
        Thread.sleep(milliSeconds);
    }
    
    private int getLivingAdjascentCellsCountAt(int xCoordinates, int yCoordinates)
    {
        int livingCellCount = 0;
        for (int x = xCoordinates - 1; x <= xCoordinates + 1; x++)
        {
            if (isOutOfBounds(x)) continue;
            
            for (int y = yCoordinates - 1; y <= yCoordinates + 1; y++)
            {
                if (isOutOfBounds(y)) continue;
                if (!cellBoard.isCellAlive(x, y)) continue;
                if (x == xCoordinates && y == yCoordinates) continue;
                
                livingCellCount++;
            }
        }
        
        return livingCellCount;
    }
    
    private boolean isOutOfBounds(int value)
    {
        return value < 0 || value >= this.boardsSize;
    }
}