package casir.lifegame;

import casir.lifegame.LifeGame;

public class Program
{
    public static void main(String[] args){
        LifeGame game = new LifeGame(20);
        game.startGame();
    }
}
