package casir.lifegame;

public class Board
{
    
    private boolean[][] board;
    private int boardSize;
    
    public Board(int boardSize)
    {
        this.board = new boolean[boardSize][boardSize];
        this.boardSize = boardSize;
        
        for (int x = 0; x < boardSize; x++)
        {
            boolean[] subArray = new boolean[boardSize];
            for (int y = 0; y < boardSize; y++)
            {
                subArray[y] = false;
            }
            
            this.board[x] = subArray;
        }
    }
    
    public boolean equals(Board otherBoard)
    {
        for (int x = 0; x < this.board.length; x++)
        {
            for (int y = 0; y < this.board.length; y++)
            {
                if (this.board[x][y] != otherBoard.board[x][y])
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    public Board clone()
    {
        Board newBoard = new Board(this.board.length);
        
        for (int x = 0; x < this.board.length; x++)
        {
            for (int y = 0; y < this.board.length; y++)
            {
                newBoard.board[x][y] = this.board[x][y];
            }
        }
        return newBoard;
    }
    
    public boolean isCellAlive(int x, int y)
    {
        return this.board[x][y];
    }
    
    public int getBoardSize()
    {
        return this.boardSize;
    }
    
    public void setAliveStateForCell(int x, int y, boolean alive)
    {
        this.board[x][y] = alive;
    }
}