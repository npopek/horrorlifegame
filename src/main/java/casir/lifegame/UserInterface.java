package casir.lifegame;

public class UserInterface
{
    
    public static void displayGameBoard(Board board)
    {
        String toDisplay;
        for (int x = 0; x < board.getBoardSize(); x++)
        {
            toDisplay = "";
            for (int y = 0; y < board.getBoardSize(); y++)
            {
                toDisplay += board.isCellAlive(x, y)
                             ? "#"
                             : "_";
            }
            
            System.out.println(toDisplay);
        }
    }
    
    public static void displayEndMessage()
    {
        System.out.println("Game over");
    }
}